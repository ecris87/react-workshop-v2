# React Workshop

This project was bootstrapped with [Create React App](https://github.com/facebookincubator/create-react-app).

## Prerequisites

Install [node](https://nodejs.org/en/download/) then run the following commands:

```
git clone https://bitbucket.org/ecris87/react-workshop-v2.git
cd react-workshop-v2
npm install
npm start
```

After all that the project should be up and running at http://localhost:3000/.

## Structure

### Session 1

#### Create-React-App

- Starting a React Project

#### Rendering

- JSX: not HTML but a syntax extension to JavaScript
- Rendering an Element into the DOM: enclosing tag, JSX fragments
- Embedding expressions in JSX
- Exploring React DOM updates
- Conditional rendering: if statements, ternary operator, logical and

#### Components

- Functional and Class components
- Working with components (composition, extraction)

### Session 2

#### Managing Data in Components

- props (read-only, can be considered parameters for components)
- state (private and fully controlled by the component)

#### State and Lifecycle

- Components lifecycle hooks
- Using state correctly: state shouldn't be modified directly, setState is asychronous, state updates are merged

#### Handling Events

- React events are named using camelCase
- Class methods are not bound by default (best practice: bind them in the constructor)

### Session 3

#### Data Fetching

- Fetching data from the server and displaying it
- Lists and keys
