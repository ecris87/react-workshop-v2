import React from 'react';
import Shop from './Shop';

/**
 * Homework
 */

const Main = () => {
  return (
    <div className="session">
      <h1>Data Fetching</h1>
      <hr />
      <h2>Example: Online shop</h2>
      <Shop />
    </div>
  );
};

export default Main;
